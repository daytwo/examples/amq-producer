package com.baustro.amqp.producer;

import javax.enterprise.context.ApplicationScoped;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import io.quarkus.logging.Log;

@ApplicationScoped
public class QuoteProcessor implements Processor {

    public void process(Exchange exchange) throws Exception {
        String message = exchange.getMessage().getBody(String.class);
        Log.info("Processing message: " + message);

        // Aqui se puede responder algo
        exchange.getIn().setBody("{'message':'Recibido OK'}");
    }
}
