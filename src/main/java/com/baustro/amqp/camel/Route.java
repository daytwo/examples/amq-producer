package com.baustro.amqp.camel;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.camel.ExchangePattern;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;

import com.baustro.amqp.model.Quote;
import com.baustro.amqp.producer.QuoteProcessor;

@ApplicationScoped
public class Route extends RouteBuilder {

    @Inject
    QuoteProcessor processor;

    @Override
    public void configure() throws Exception {
        restConfiguration().bindingMode(RestBindingMode.json);

        //@formatter:off
        rest("/queue").
          post().
            type(Quote.class).
            to("direct:sendtoamq");
        //@formatter:on

        //@formatter:off
        from("direct:sendtoamq").
        routeId("FromRest2Amq").
        log("Body : \"${body}\"").
        convertBodyTo(String.class).
        to(ExchangePattern.InOnly, "amqp:queue:test-queue").
        process(processor);
        //@formatter:on
    }
}
